import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {

    public static  JTextArea TF1,TF2;
    public static JButton btn;
    S2()
    {
        setTitle("Exercitiu 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setLocation(400,400);
        init();
        setVisible(true);
    }
    public void init()
    {
        this.setLayout(null);
        TF1=new JTextArea();
        TF1.setBounds(50,50,300,50);

        TF2=new JTextArea();
        TF2.setBounds(50,150,300,50);

        btn=new JButton("btn");
        btn.setBounds(200,250,100,100);
        btn.addActionListener(new Convert());


        add(TF1);add(TF2);add(btn);
    }

    class Convert implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) {
            String s1= new String();
            s1=TF1.getText();
            char c[]=new char[s1.length()];
            for (int i=0;i<s1.length();i++)
            {
                c[i]='a';
            }
            String s2= new String(c);
            TF2.setText(s2);
        }
    }
    public static void main(String[] args)
    {
       new S2();
    }
}
